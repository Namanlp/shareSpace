# Conky Config

This Conky config is highly customized combination of conky configurations. A part of this has been adapted from <a href="https://store.kde.org/p/1197920"> SciFi_Conky_HUD </a> and  <a href="https://www.gnome-look.org/p/1460295/"> Conky Rock and Roll </a>


## Setting Up

First install conky-manager for your distro. Then, install Anurati font (Anurati.otf). Then copy .conky folder in your home folder along with conky_start.sh. Finally, set conky_start.sh to auto start using your Desktop Environment.

## Sample ScreenShot

<img src="SS.png" />
