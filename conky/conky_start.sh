#!/bin/bash

killall conky

conky -c ~/.conky/user.rc
conky -c ~/.conky/video.rc
conky -c ~/.conky/net.rc
conky -c ~/.conky/mem.rc
conky -c ~/.conky/circle_anim.rc
conky -c ~/.conky/info.rc 
conky -c ~/.conky/hdd.rc
conky -c ~/.conky/calender.rc
